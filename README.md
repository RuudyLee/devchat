For the purpose of this assignment, we have created two separate repositories to keep it easy to manage by the group members. This repository was set up to create** the Server** of the DevChat. There is a separate repository just for the client. 

The group project is by:
**Name          | Username          | Student number**
___________________________________________________
Ishraf Uddin (100520689),  100520689 | 
Jonathan Umar-Khitab (UkStormDragon), 100520306 | 
Nour Halabi (Nour_Halabi), 100525020 |
Rudy Lee (RuudyLee), 100425280 |

___________________________________________________

To run this project, first run the server and then run the client. In the client, select your icon, type in your name and then hit the join button. There will be two tabs: the first tab displays the chat system and the other tab holds the file sharing system.