import javafx.collections.ObservableList;

import java.awt.*;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.Socket;
import java.net.UnknownHostException;
import java.util.Observable;

/**
 * Created by ishraf222 on 07/04/16.
 */
public class DevChatClient extends Frame {

    private Socket socket = null;
    private BufferedReader in = null;
    private PrintWriter networkOut = null;
    private BufferedReader networkIn = null;

    public static String SERVER_ADDRESS = "localhost";
    public static int SERVER_PORT = 8888;
    public String username = "";
    public int picIndex = -1;



    public DevChatClient(int picIndex, String userName)
    {
        this.username = username;
        this.picIndex = picIndex;

        try {
            socket = new Socket(SERVER_ADDRESS, SERVER_PORT);
        } catch (UnknownHostException e) {
            System.err.println("Unknown host: " + SERVER_ADDRESS);
        } catch (IOException e) {
            System.err.println("IOException while connecting to server: " + SERVER_ADDRESS);
        }

        // open read/write connection
        try {
            networkOut = new PrintWriter(socket.getOutputStream(), true);
            networkIn = new BufferedReader(new InputStreamReader(socket.getInputStream()));
        } catch (IOException e) {
            System.err.println("IOException while opening a read/write connection");
        }

        in = new BufferedReader(new InputStreamReader(System.in));

    }

    public void addMsg(String outMsg)
    {
        networkOut.println(picIndex + " " + username + " " + outMsg);
    }

    public void retrieveMsg(ObservableList<String> poop)
    {

    }


}
